package com.unicom.sailcall.annotation;

import java.lang.annotation.*;

/**
 * 登录验证
 * @outher larry
 * @create 2018/09/12
 */

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Login {
    Role[] value() default Role.VIEW;
    boolean required() default true;
}
