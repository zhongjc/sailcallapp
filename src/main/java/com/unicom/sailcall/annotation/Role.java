package com.unicom.sailcall.annotation;

/**
 * 角色
 */
public enum Role {
    //管理员
    ADMIN,

    // 只读
    VIEW,
    ;
}
