package com.unicom.sailcall.exception;


import com.unicom.sailcall.model.Result;

/**
 * 业务级异常
 * @outher larry
 * @create 2011/09/07
 */

public class AppException extends RuntimeException {

    private Result result;

    public AppException(Result result) {
        this.result = result;
    }

    public Result getResult() {
        return result;
    }
}
