package com.unicom.sailcall.model;

/**
 *
 * Created by larry on 11/27/13.
 */

public enum MODEL {
    EMAIL(1, "邮箱"),
    NICKNAME(2, "用户名"),
    PASSWORD(3, "密码"),
    VCODE(4, "验证码"),
    LOGIN(5, "登录"),
    SIGNUP(6, "注册"),
    REQUEST(7, "请求"),
    FILE(8, "文件"),
    PROGRESS(9,"执行过程"),
    CONTENT(10,"内容"),
    PHONENUM(11,"电话号码"),
    ;

    MODEL(int code, String msg) {
        this.code =code;
        this.msg = msg;
    }

    private final int code;
    private final String msg;

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }
}
