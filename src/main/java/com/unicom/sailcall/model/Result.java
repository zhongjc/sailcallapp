package com.unicom.sailcall.model;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

/**
 * Created by larry on 11/27/13.
 */
@Data
public class Result {

    /**
     * 请求id
     */
    private String requestId;

    /**
     * 响应信息
     */
    private String msg;

    /**
     * 服务端处理时间 unixTime
     */
    private long time;

    /**
     * 状态码
     */
    private int code;

    /**
     * 返回的数据
     */
    private Object data;


    public Result() {
        this.time = Instant.now().getEpochSecond();
        this.requestId = UUID.randomUUID().toString().replace("-", "");
    }

    public Result(int code, String msg, Object data) {
        this();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Result(StatusCode.Status status) {
        this(status.getCode(), status.getMsg(), "");
    }

    public static Result fail(String msg, Object data) {
        return new Result(400, msg, data);
    }

    public static Result success(String msg, Object data) {
        return new Result(200, msg, data);
    }

    public static Result success(Object data) {
        return new Result(200, "success", data);
    }

    public static Result success() {
        return new Result(200, "ok", "");
    }

    public static Result fail(MODEL model, ERROR error, String msg) {
        int code = StatusCode.formatCode(StatusCode.LEVEL.APP, model, error);
        return new Result(new StatusCode.Status(code, msg));
    }

    public static Result fail(MODEL model, ERROR error) {
        return new Result(StatusCode.formatStatus(StatusCode.LEVEL.APP, model, error));
    }


    public boolean isSuccess() {
        return this.code == 200 || this.code == 0;
    }
}
