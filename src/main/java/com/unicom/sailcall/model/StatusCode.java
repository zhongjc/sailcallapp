package com.unicom.sailcall.model;

import org.apache.commons.lang3.math.NumberUtils;

import java.text.DecimalFormat;
import java.text.MessageFormat;

/**
 * ABBCC = 10305 #错误代码由5位组成
 * # 其中
 * A=1    #一位数 1:系统级错误 2:应用级错误
 * BB=03  #两位 表示模块代码 03:表示密码
 * CC=05  #两位 表示具体错误 05:表示不能为空
 * #所以
 * # 10305 表示： 密码不能为空
 * # 20405 表示:  验证码不能为空
 * <p>
 * # 特殊状态吗如下：
 * 200 # 表示处理正常
 *
 * Created by larry on 11/27/13.
 */
public class StatusCode {
    public enum LEVEL {
        SERVER {
            @Override
            public int getCode() {
                return 1;
            }
        },
        APP {
            @Override
            public int getCode() {
                return 2;
            }
        };

        public abstract int getCode();
    }

    public static class Status {
        public int code;
        public String msg;

        public Status(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

    public static int formatCode(LEVEL level, MODEL model, ERROR error) {
        DecimalFormat format = new DecimalFormat("00");
        String str = MessageFormat.format("{0}{1}{2}",
                level.getCode(),
                format.format(model.getCode()),
                format.format(error.getCode()));
        return NumberUtils.toInt(str, -1);
    }

    public static Status formatStatus(LEVEL level, MODEL model, ERROR error) {
        int code = formatCode(level, model, error);
        String msg = MessageFormat.format("{0}{1}", model.getMsg(), error.getMsg());
        return new Status(code, msg);
    }

    public static Status formatStatus(LEVEL level, MODEL model, ERROR error, String overMsg) {
        int code = formatCode(level, model, error);
        return new Status(code, overMsg);
    }

    public static Status formatStatus(int level, int model, int error, String overMsg) {
        DecimalFormat format = new DecimalFormat("00");
        String str = MessageFormat.format("{0}{1}{2}", level, format.format(model), format.format(error));
        int code = NumberUtils.toInt(str, -1);
        return new Status(code, overMsg);
    }

    /**
     * APP应用级错误级别
     *
     * @param MODEL
     * @param error
     * @return
     */
    public static Status formatStatus(MODEL MODEL, ERROR error) {
        int code = formatCode(LEVEL.APP, MODEL, error);
        String msg = MessageFormat.format("{0}{1}", MODEL.getMsg(), error.getMsg());
        return new Status(code, msg);
    }


    /**
     * APP应用级错误级别
     *
     * @param MODEL
     * @param error
     * @return
     */
    public static Status formatStatus(MODEL MODEL, ERROR error, String overMsg) {
        int code = formatCode(LEVEL.APP, MODEL, error);
        return new Status(code, overMsg);
    }

    public static Status getOKStatus(String overMsg) {
        return new Status(200, overMsg);
    }
}
