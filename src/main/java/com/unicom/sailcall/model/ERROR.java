package com.unicom.sailcall.model;

/**
 *
 * Created by larry on 11/27/13.
 */

public enum ERROR {

    NOT_FOUND(1, "未找到"),
    NOT_NULL(2, "不能为空"),
    NOT_CORRECT(3, "不正确"),
    FAILED(4, "失败"),
    NO_PERMISSION(5, "没有权限"),
    PARAMETER_ERROR(6, "参数错误"),
    NOT_SUPPORT_METHOD(7, "不支持该方法"),
    PROCESS_ERROR(8, "执行出错"),
    INVALID(9, "失效"),
    EXISTED(10, "已存在"),
    NEED_ACTIVE(11, "需要激活"),
    FORMAT_ERROR(12, "格式错误"),
    OUT_TIMES(13, "发送次数过多"),
    UNBIND(14, "未绑定"),
    ACTIVED(15, "已经激活"),
    NOT_PERFECTION(16, "信息不完善"),
    CREATE_FAILED(17, "创建失败"),
    BIND(18, "已经绑定"),
    DISABLE(19, "禁用"),
    ENABLED(20, "已启用"),
    UPDATE(21, "更新失败"),
    OVER_SIZE(22, "超过限制大小"),
    TIME_OUT(23, "超时"),
    NOT_SUPPORT(24, "不支持"),
    IS_NULL(25, "为空"),
    PWD_INCORRECT(26, "错误"),
    UNAME_NULL(27, "不能为空"),
    UNAME_NOT_EXISTS(28, "不存在")

    ;

    private final int code;
    private final String msg;

    ERROR (int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}

