package com.unicom.sailcall.service;

import com.unicom.sailcall.model.Result;

public interface AssistService {
    Result busyCheck(Integer sailorId);

    void monthlyBill();
}
