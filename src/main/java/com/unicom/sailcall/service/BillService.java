package com.unicom.sailcall.service;

import com.unicom.sailcall.entity.Bill;

import java.util.List;

public interface BillService {
    List<Bill> getUserBills(Integer userId);
}
