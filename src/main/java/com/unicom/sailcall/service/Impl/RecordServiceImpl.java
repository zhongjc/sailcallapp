package com.unicom.sailcall.service.Impl;

import com.unicom.sailcall.dao.BillDao;
import com.unicom.sailcall.dao.RecordDao;
import com.unicom.sailcall.dao.UserDao;
import com.unicom.sailcall.entity.Bill;
import com.unicom.sailcall.entity.Record;
import com.unicom.sailcall.entity.User;
import com.unicom.sailcall.entity.returnData.RecentRecord;
import com.unicom.sailcall.entity.returnData.RecordDetail;
import com.unicom.sailcall.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class RecordServiceImpl implements RecordService {
    @Autowired
    private RecordDao recordDao;

    @Autowired
    private BillDao billDao;

    @Autowired
    private UserDao userDao;

    @Override
    public Integer makeRecord(Record record) {
        //得到一条新的通话记录
        /**
         * record包含的信息有: source, dest, status, startTime, endTime, charge
         * 更新bill需要的信息有: userId, month, duration, expense
         */
        //检查数据有效性
        Integer integer = checkNull4Bill(record);

        if(integer == 0)
            return null;

        if(integer == 1)
            return recordDao.insertOne(record);

        //更新本月账单
        User user = userDao.selectUserByPhone(record.getSource());
        Integer id = user.getId();
        Integer month = new Integer(new SimpleDateFormat("yyyyMM").format(new Date()));
        long duration = record.getEndTime() - record.getStartTime();
        Bill bill = new Bill();
        bill.setUserId(id);
        bill.setMonth(month);
        bill.setDuration(duration);
        bill.setExpense(record.getCharge());
        billDao.updateBill(bill);

        return recordDao.insertOne(record);
    }

    @Override
    public void balance(Integer recordId, Long endTime) {
    //TODO 通话双方的话费计算m------>只有打电话需要话费

    }

    @Override
    public List<RecordDetail> MonthlyRecordDetail(String phonenum, Integer month) {
        List<Record> monthlyRecord = recordDao.getMonthlyRecord(phonenum, month);
        List<RecordDetail> recordDetails = new ArrayList<>();
        for (Record record : monthlyRecord) {
            RecordDetail recordDetail = record2RecordDetail(record);
            if(recordDetail == null)
                continue;
            recordDetails.add(recordDetail);
        }
        return recordDetails;
    }

    @Override
    public List<RecentRecord> getRecentRecords(String phonenum) {
        Long aMonth = new Date().getTime()/1000 - 60 * 60 * 24 * 30;  //最近一个月的时间,以30天为一个月
        List<Record> records = recordDao.getRecentRecords(phonenum, aMonth);
        ArrayList<RecentRecord> recentRecords = new ArrayList<>();

        for (Record record : records) {
            if(record.getDest() == null || record.getStartTime() == null || record.getSource() == null)
                continue;

            RecentRecord recentRecord = new RecentRecord();
            recentRecord.setId(record.getId());
            recentRecord.setSource(record.getSource());
            recentRecord.setDest(record.getDest());
            recentRecord.setStatus(record.getStatus());

            recentRecord.setCallTime(long2String(record.getStartTime()));
            if(record.getStatus() == 0){  //有效的通话记录
                if(record.getStartTime() == null || record.getEndTime() == null)
                    recentRecord.setDuration("统计错误");
                else {
                    recentRecord.setDuration(seconds2String(record.getEndTime() - record.getStartTime()));
                }
            }else{
                recentRecord.setDuration("null");
            }

            recentRecords.add(recentRecord);
        }
        return recentRecords;
    }

    /**
     * @param record
     * @return
     * 0 : 不需要记录此条记录
     * 1 : 记录，但不加入到账单
     * 2 : 正常返回
     */
    private Integer checkNull4Bill(Record record){
        if(record.getSource() == null)
            return 0;

        if(record.getStartTime() == null || record.getEndTime() == null)
            return 1;

        if(record.getStatus() == 0 && record.getCharge() == null)
            record.setCharge(0D);

        return 2;
    }

    /**
     * 将record类型的数据封装成recordDetail类型
     * @param record
     * @return RecordDetail
     */
    private RecordDetail record2RecordDetail(Record record){
        if(record.getSource() == null
                || record.getStatus() != 0
                || record.getDest() == null)
            return null;

        RecordDetail recordDetail = new RecordDetail();
        recordDetail.setId(record.getId());
        recordDetail.setSource(record.getSource());
        recordDetail.setDest(record.getDest());
        recordDetail.setCharge(record.getCharge());
        recordDetail.setStatus(record.getStatus());
        if (record.getCharge() == null)
             recordDetail.setCharge(0D);
        if(record.getStartTime() == null || record.getEndTime() == null)
            recordDetail.setDuration("统计错误");
        else {
            Long intDuration = record.getEndTime() - record.getStartTime();
            recordDetail.setDuration(seconds2String(intDuration));
        }

         if(record.getStartTime() != null){
             recordDetail.setCallTime(seconds2Time(record.getStartTime()));
         }

         return recordDetail;
    }

    public static String seconds2String(Long intDuration){
        String DateTimes;
        long days = intDuration / ( 60 * 60 * 24);
        long hours = (intDuration % ( 60 * 60 * 24)) / (60 * 60);
        long minutes = (intDuration % ( 60 * 60)) /60;
        long seconds = intDuration % 60;
        if(days > 0){
            DateTimes= days + "天" + hours + "时" + minutes + "分" + seconds + "秒";
        }else if(hours > 0){
            DateTimes=hours + "时" + minutes + "分" + seconds + "秒";
        }else if(minutes > 0){
            DateTimes = minutes + "分" + seconds + "秒";
        }else{
            DateTimes = seconds + "秒";
        }
        return DateTimes;
    }

    private String seconds2Time(Long startTime){
        String format = new SimpleDateFormat("yyyy/MM/dd HH:mm").format(startTime * 1000);  //变成毫秒数
        if(format.charAt(5) == '0'){
            format = format.substring(0,5) + format.substring(6);
        }
        String year = new SimpleDateFormat("yyyy").format(new Date());
        if(format.startsWith(year))
            return format.substring(5);

        return format;
    }

    private String long2String(Long time){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        long today = cal.getTime().getTime()/1000;

        if(time >= today)
            return "今天 " + format.format(time * 1000);

        cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - 1);

        long yesterday = cal.getTime().getTime()/1000;

        if(time >= yesterday)
            return "昨天 " + format.format(time * 1000);

        Calendar cal2 = Calendar.getInstance();
        int day_of_week = cal2.get(Calendar. DAY_OF_WEEK) - 1;
        if (day_of_week == 0 ) {
            day_of_week = 7 ;
        }
        cal2.add(Calendar.DATE , -day_of_week + 1 );
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.MILLISECOND, 0);

        long week = cal2.getTime().getTime()/1000;

        if(time >= week){
            String[] weekDays = { "周日", "周一", "周二", "周三", "周四", "周五", "周六" };
            cal.setTime(new Date(time * 1000));
            int w = cal.get(Calendar.DAY_OF_WEEK) - 1; // 指示一个星期中的某天。
            if (w < 0)
                w = 0;
            return weekDays[w] + " " + format.format(time * 1000);
        }

        cal2.setTime(new Date());
        int year = cal2.get(Calendar.YEAR);
        cal2.setTime(new Date(time * 1000));
        int year1 = cal2.get(Calendar.YEAR);

        if(year == year1) {//如果年相同，不显示年
            SimpleDateFormat format1 = new SimpleDateFormat("MM/dd HH:mm");
            return format1.format(time * 1000);
        }
        //如果不同，显示年
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        return format1.format(time * 1000);
    }
}
