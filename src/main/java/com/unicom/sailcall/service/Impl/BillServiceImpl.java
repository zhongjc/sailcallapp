package com.unicom.sailcall.service.Impl;

import com.unicom.sailcall.dao.BillDao;
import com.unicom.sailcall.entity.Bill;
import com.unicom.sailcall.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS)
public class BillServiceImpl implements BillService {
    @Autowired
    private BillDao billDao;

    @Override
    public List<Bill> getUserBills(Integer userId) {
        return billDao.getMonthlyBill(userId);
    }
}
