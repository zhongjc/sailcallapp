package com.unicom.sailcall.service.Impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.unicom.sailcall.annotation.Role;
import com.unicom.sailcall.dao.UserDao;
import com.unicom.sailcall.entity.User;
import com.unicom.sailcall.entity.cache.CacheInfo;
import com.unicom.sailcall.exception.AppException;
import com.unicom.sailcall.model.ERROR;
import com.unicom.sailcall.model.MODEL;
import com.unicom.sailcall.model.Result;
import com.unicom.sailcall.service.UserService;
import com.unicom.sailcall.utils.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    @Transactional( propagation = Propagation.SUPPORTS)
    public Result login(User user, HttpServletResponse response) {
        if(user.getPhone() == null){
            return Result.fail(MODEL.PHONENUM, ERROR.UNAME_NULL);
        }else {
            User resultUser = userDao.selectUserByPhone(user.getPhone());

            if(resultUser == null){
                return Result.fail(MODEL.PHONENUM, ERROR.NOT_FOUND);
            }else if(resultUser.getStatus() ==  1){ //冻结状态
                return Result.fail(MODEL.PHONENUM, ERROR.DISABLE );
            }else if(! resultUser.getPassword().equals(user.getPassword())){
                return Result.fail(MODEL.PASSWORD, ERROR.PWD_INCORRECT);
            }
            //将用户信息缓存
            RequestUtil.cache(response, new CacheInfo(resultUser.getName(), Role.ADMIN, resultUser));

            return Result.success("登录成功");
        }
    }

    @Override
    public Result resetUser(User user) {
        try{
            userDao.setPassword(user.getPassword(), user.getPhone());
            return Result.success();
        }catch (Exception e){
            return Result.fail(MODEL.PROGRESS,ERROR.PROCESS_ERROR);
        }
    }

    @Override
    public Result check(HttpServletRequest request) {
        CacheInfo user = RequestUtil.getCacheInfo(request);
        // 登录失效
        if (user == null) {
            throw new AppException(Result.fail(MODEL.LOGIN, ERROR.INVALID));
        }
        User o = JSON.parseObject( JSONObject.toJSONString(user.getO()), User.class);
        o.setPassword(null);

        return Result.success(o);
    }
}
