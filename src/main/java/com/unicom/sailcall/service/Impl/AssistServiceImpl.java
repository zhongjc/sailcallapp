package com.unicom.sailcall.service.Impl;

import com.unicom.sailcall.dao.BillDao;
import com.unicom.sailcall.dao.RecordDao;
import com.unicom.sailcall.dao.UserDao;
import com.unicom.sailcall.entity.Bill;
import com.unicom.sailcall.entity.User;
import com.unicom.sailcall.model.Result;
import com.unicom.sailcall.service.AssistService;
import com.unicom.sailcall.utils.Loggers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class AssistServiceImpl implements AssistService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private BillDao billDao;

    @Autowired
    private RecordDao recordDao;


    @Override
    public Result busyCheck(Integer sailorId) {


        return null;
    }

    @Override
    public void monthlyBill() {
        //得到当前月份 形式为 201902
        String month = new SimpleDateFormat("yyyyMM").format(new Date());

        String previousMonth = getPreviousMonth(month);
        Loggers.APP.info("定时任务：为上个月 {} 定时创建账单", previousMonth);

        //得到所有用户的信息
        List<User> users = userDao.getUserInfo();
        Loggers.APP.info("定时任务: 总共 {} 位用户", users.size());
        int count =1;
        for (User user : users) {
            String phonenum = user.getPhone();
            try{
                Bill monthlyBill = recordDao.getMonthlyBill(phonenum, new Integer(previousMonth));
                monthlyBill.setStatus(0);
                System.out.println(monthlyBill);
                billDao.updateMonthlyBill(monthlyBill);
            }catch (Exception e){
                Loggers.APP.info("定时任务: 进行到第{}个用户,手机号:{} 时失败，总共 {} 个用户", count, phonenum, users.size());
                //TODO 失败后有什么应对策略
                e.printStackTrace();
            }finally {
                count++;

                //在数据库中添加这个月的账单条目
                Bill bill = new Bill();
                bill.setUserId(user.getId());
                bill.setMonth(new Integer(month));
                bill.setDuration(0L);
                bill.setExpense(0D);
                billDao.generateMonthlyBill(bill);
            }
        }
    }

    private String getPreviousMonth(String month){
        Integer previousMonth = new Integer(month.substring(4)) - 1;
        Integer year = new Integer(month.substring(0,4));

        if(previousMonth == 0)
            return --year + "12";

        if(previousMonth<10)
            return year + "0" + previousMonth;

        return year + "" + previousMonth;
    }
}
