package com.unicom.sailcall.service;

import com.unicom.sailcall.entity.Record;
import com.unicom.sailcall.entity.returnData.RecentRecord;
import com.unicom.sailcall.entity.returnData.RecordDetail;

import java.util.List;

public interface RecordService {
    //TODO 待完善
    Integer makeRecord(Record record);  //返回主键
    void balance(Integer recordId, Long endTime); //计算一次通话的结束时间，消费金额

    List<RecordDetail> MonthlyRecordDetail(String phonenum, Integer month);

    List<RecentRecord> getRecentRecords(String phonenum);
}
