package com.unicom.sailcall.service;

import com.unicom.sailcall.entity.User;
import com.unicom.sailcall.model.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface UserService {
    Result login(User user, HttpServletResponse response);
    Result resetUser(User user);
    Result check(HttpServletRequest request);
}
