package com.unicom.sailcall.common;

import com.unicom.sailcall.entity.cache.CacheInfo;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * 常量类
 */

@Component
public class Constant {

    /**
     * 当前线程内登录的用户
     */
    public final static ThreadLocal<CacheInfo> CURRENT_CACHE = new ThreadLocal<>();


    /**
     *  请求体中 user 信息
     */
    public final static String REQ_CURRENT_USER = "sys__user";


    /**
     * session 中用户信息
     */
    public static String SESSION_USER_INFO = "_u";


    /**
     * 支持的视频
     */
    public final static List<String> SUPPORT_VIDEO_FORMAT = new ArrayList<>(2);


    @PostConstruct
    public void init() {
        SUPPORT_VIDEO_FORMAT.add("mp4");
        SUPPORT_VIDEO_FORMAT.add("avi");
    }
}
