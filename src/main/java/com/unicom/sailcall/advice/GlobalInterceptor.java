package com.unicom.sailcall.advice;

import com.unicom.sailcall.annotation.Login;
import com.unicom.sailcall.annotation.Role;
import com.unicom.sailcall.common.Constant;
import com.unicom.sailcall.entity.cache.CacheInfo;
import com.unicom.sailcall.exception.AppException;
import com.unicom.sailcall.model.ERROR;
import com.unicom.sailcall.model.MODEL;
import com.unicom.sailcall.model.Result;
import com.unicom.sailcall.utils.Loggers;
import com.unicom.sailcall.utils.RequestUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @outher larry
 * @create 2018/09/06
 */

@Component
public class GlobalInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        return auth(request, handler);
    }
    /**
     * 总的拦截入口
     * @param request
     * @param handler
     * @return
     * @throws Exception
     */
    public boolean auth(HttpServletRequest request, Object handler) throws Exception {
        String path = request.getRequestURI();
        Loggers.APP.info("前置拦截 {} {}", request.hashCode(), path);

        if ( ! (handler instanceof HandlerMethod) ) {
            return true;
        }


        Login login = ((HandlerMethod)handler).getMethod().getAnnotation(Login.class);
        if (login == null) {
            login = ((HandlerMethod) handler).getMethod().getDeclaringClass().getAnnotation(Login.class);
        }

        // 不需要登录
        if (login == null) {
            return true;
        }
        if (!login.required()) {
            return true;
        }

        CacheInfo user = RequestUtil.getCacheInfo(request);

        // 没有登录
        if (user == null) {
            throw new AppException(Result.fail(MODEL.LOGIN, ERROR.INVALID));
        }

        // 保存用户信息到当前请求
        request.setAttribute(Constant.REQ_CURRENT_USER, user);
        Constant.CURRENT_CACHE.set(user);

        // 验证管理员账号
        if (ArrayUtils.contains(login.value(), Role.ADMIN)) {
            if (user.getRole() == null || user.getRole().equals(Role.VIEW)) {
                throw new AppException(Result.fail(MODEL.REQUEST, ERROR.NO_PERMISSION));
            }
        }
        return true;
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        CacheInfo cacheInfo =Constant.CURRENT_CACHE.get();
        if (cacheInfo != null) {
           Loggers.OPERATE.info("{} {} {} {}", request.getMethod(), request.getRequestURI(), cacheInfo.getName());
        }

        Constant.CURRENT_CACHE.remove();
    }
}
