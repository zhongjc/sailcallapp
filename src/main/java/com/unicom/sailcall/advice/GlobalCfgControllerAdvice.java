package com.unicom.sailcall.advice;

import com.unicom.sailcall.exception.AppException;
import com.unicom.sailcall.model.ERROR;
import com.unicom.sailcall.model.MODEL;
import com.unicom.sailcall.model.Result;
import com.unicom.sailcall.model.StatusCode;
import com.unicom.sailcall.utils.Loggers;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 全局默认 controller 配置
 *
 * 参考
 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
 *
 * @outher larry
 * @create 2018/09/05
 */

@RestControllerAdvice
public class GlobalCfgControllerAdvice {

    /**
     * 拦截全局错误栈
     * @param e
     * @return
     */
    @ExceptionHandler({Throwable.class, })
    @ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE)
    public Result handleException(Exception e) {
        Loggers.APP.error("System.Err ", e);

        return new Result(StatusCode.formatStatus(StatusCode.LEVEL.SERVER,
                MODEL.REQUEST, ERROR.PROCESS_ERROR));
    }

    @ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Result httpRequestMethodNotSupported() {
        return Result.fail(MODEL.REQUEST, ERROR.NOT_SUPPORT_METHOD);
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public Result noHandlerFound() {
        return Result.fail(MODEL.REQUEST, ERROR.NOT_FOUND);
    }

    /**
     * 系统内部异常处理
     * @param e
     * @return
     */
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(AppException.class)
    public Result appException(AppException e) {
        Loggers.APP.error("App.Err {} {}", e.getResult() , e);

        if (e.getResult() != null) {
            return e.getResult();
        }

        return Result.fail(MODEL.REQUEST, ERROR.PROCESS_ERROR);
    }


    /**
     * 上传最大文件异常处理
     * @return
     */
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public Result maxUploadSizeLimit() {
        return Result.fail(MODEL.FILE, ERROR.OVER_SIZE);
    }


}
