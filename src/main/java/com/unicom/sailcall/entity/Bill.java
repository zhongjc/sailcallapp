package com.unicom.sailcall.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Bill {
    private Integer id;
    private Integer userId;
    private Integer month;
    private Long duration;
    private Double expense;
    private Integer status;

    private String phonenum;

}
