package com.unicom.sailcall.entity.cache;

import com.unicom.sailcall.annotation.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @outher larry
 * @create 2018/09/13
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CacheInfo implements Serializable {
    private String name;
    private Role role;
    private Object o;
}