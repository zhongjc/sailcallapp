package com.unicom.sailcall.entity.returnData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RecordDetail {
    private Integer id;
    private String source;
    private String dest;
    private String callTime;
    private String duration;
    private Double charge;
    private Integer status;
}
