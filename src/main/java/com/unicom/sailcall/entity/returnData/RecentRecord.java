package com.unicom.sailcall.entity.returnData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RecentRecord {
    private Integer id;
    private String source;
    private String dest;
    private Integer status;
    private String callTime;
    private String duration;
}
