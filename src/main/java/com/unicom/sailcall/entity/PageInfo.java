package com.unicom.sailcall.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
    用来在分页查询所有信息的时候显示分页信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageInfo {
    private int pageNum;    //页数
    private int pageSize;   //每页条数
    private int startRow;   //开始
    private int endRow;     //结束
    private int pages;      //总页数
}
