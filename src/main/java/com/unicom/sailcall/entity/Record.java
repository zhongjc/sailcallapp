package com.unicom.sailcall.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Record {
    private Integer id;
    private String source;
    private String dest;
    private Integer status;
    private Long startTime;
    private Long endTime;
    private Double charge;
}
