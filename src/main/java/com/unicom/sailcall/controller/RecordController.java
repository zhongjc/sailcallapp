package com.unicom.sailcall.controller;

import com.unicom.sailcall.entity.returnData.RecentRecord;
import com.unicom.sailcall.entity.returnData.RecordDetail;
import com.unicom.sailcall.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/record")
public class RecordController {
    @Autowired
    private RecordService recordService;

    @RequestMapping("/recordDetail")
    public List<RecordDetail> monthlyRecordDetail(String phonenum, Integer month){
        return recordService.MonthlyRecordDetail(phonenum, month);
    }

    @RequestMapping("/recentRecord")
    public List<RecentRecord> recentRecordsList(String phonenum){
        return recordService.getRecentRecords(phonenum);
    }
}
