package com.unicom.sailcall.controller;

import com.unicom.sailcall.entity.User;
import com.unicom.sailcall.model.Result;
import com.unicom.sailcall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public Result login(User user, HttpServletResponse response){
        return userService.login(user, response);
    }

    @RequestMapping("/setPwd")
    public Result setPassword(User user){
        return userService.resetUser(user);
    }

    @RequestMapping("/check")
    public Result checkValid(HttpServletRequest request){
        return userService.check(request);
    }

    //TODO 添加一名新的用户时要在bill表内创建他本月的账单
}
