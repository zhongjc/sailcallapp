package com.unicom.sailcall.controller;

import com.unicom.sailcall.model.Result;
import com.unicom.sailcall.service.AssistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/assist")
public class AssistController {
    @Autowired
    private AssistService assistService;

    @RequestMapping("/checkBusy")
    public Result busyCheck(Integer sailorId){
       return assistService.busyCheck(sailorId);
    }

    /**
     * 每月1号为每一个用户生成账单
     * 每月的1日的凌晨2点调整任务
     * 每分钟：0 * * * * *
     */
    @Scheduled(cron = "0 0 2 1 * *")
    private void monthlyBill(){
        assistService.monthlyBill();
    }

}
