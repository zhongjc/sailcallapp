package com.unicom.sailcall.controller;

import com.unicom.sailcall.entity.Bill;
import com.unicom.sailcall.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/bill")
public class BillController {
    @Autowired
    private BillService billService;


    @RequestMapping("/userBills")
    public List<Bill> getUserBills(Integer userId){
        return billService.getUserBills(userId);
    }
}
