package com.unicom.sailcall.dao;

import com.unicom.sailcall.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserDao {
    User selectUserByPhone(String phone);
    void setPassword(@Param("password") String password, @Param("phone") String phone);

    List<User> getUserInfo();
}
