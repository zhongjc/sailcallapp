package com.unicom.sailcall.dao;

import com.unicom.sailcall.entity.Bill;
import com.unicom.sailcall.entity.Record;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RecordDao {
    Integer insertOne(Record record);
    void updateOne(Record record);

    Bill getMonthlyBill(@Param("phonenum") String phonenum, @Param("month") Integer month); //month 的格式限制为201901

    List<Record> getMonthlyRecord(@Param("phonenum") String phonenum, @Param("month")Integer month); //month 的格式为201902

    List<Record> getRecentRecords(@Param("phonenum") String phonenum, @Param("aMonth") Long aMonth); //aMonth 的格式是秒数
}
