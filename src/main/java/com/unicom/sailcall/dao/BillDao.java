package com.unicom.sailcall.dao;

import com.unicom.sailcall.entity.Bill;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BillDao {
    void generateMonthlyBill(Bill bill);
    void updateBill( Bill bill); //用于在有新的扣费项目产生时，更新每月账单
    void updateMonthlyBill(Bill bill);  //用于在每月一号时重新统计上月账单

    List<Bill> getMonthlyBill(Integer userId);
}
