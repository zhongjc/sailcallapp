package com.unicom.sailcall.utils;

import com.github.pagehelper.Page;
import com.unicom.sailcall.entity.PageInfo;


/**
 * 关于分页信息的处理
 */
public class PageUtil {

    public static PageInfo getPageInfo(Page page){

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(page.getPageSize());
        pageInfo.setPageNum(page.getPageNum());
        pageInfo.setPages(page.getPages());
        pageInfo.setStartRow(page.getStartRow());
        pageInfo.setEndRow(page.getEndRow());
        return pageInfo;
    }
}
