package com.unicom.sailcall.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtil {

    private static TimeZone china = TimeZone.getTimeZone("GMT+8");

    /**
     * 将 long 类型的毫米数转成时间 yyyyMMddHHmmss
     *
     * @param time
     * @return
     */
    public static Long getTime(Long time) {
        Calendar c = Calendar.getInstance();
        c.setTimeZone(china);
        c.setTimeInMillis(time);
        String yearStr = c.get(Calendar.YEAR) + "";//获取年份
        int month = c.get(Calendar.MONTH) + 1;//获取月份
        String monthStr = month < 10 ? "0" + month : month + "";
        int day = c.get(Calendar.DATE);//获取日
        String dayStr = day < 10 ? "0" + day : day + "";
        int hours = c.get(Calendar.HOUR_OF_DAY);//获取小时,24小时制
        String hoursStr = hours < 10 ? "0" + hours : hours + "";
        int minutes = c.get(Calendar.MINUTE);//获取分钟
        String minutesStr = minutes < 10 ? "0" + minutes : minutes + "";
        int seconds = c.get(Calendar.SECOND);//获取秒数
        String secondsStr = seconds < 10 ? "0" + seconds : seconds + "";
        return Long.valueOf(yearStr + monthStr + dayStr + hoursStr + minutesStr + secondsStr);
    }

    /**
     * 将 20181226 转成 Long 类型的毫秒数
     *
     * @param time
     * @return
     */
    public static Long getMillSecond(Long time) {

        if (isValidDate(String.valueOf(time))) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                sdf.setTimeZone(china);
                Date date = sdf.parse(String.valueOf(time));
                return date.getTime();
            } catch (ParseException e) {
                Loggers.APP.warn("error {}", e);
                return null;
            }
        }
        Loggers.APP.warn("The date {} format error , Please input the right data like yyyyMMdd ", time);
        return null;
    }

    /**
     * 将 long 类型的毫秒数转成时间 yyyyMMdd
     * 由于服务器时间并不是东八区的时间，因此要进行时区的转换
     *
     * @param time
     * @return
     */
    public static Long getYearHourDay(Long time) {

        Calendar c = Calendar.getInstance();
        c.setTimeZone(china);
        c.setTimeInMillis(time);
        String yearStr = c.get(Calendar.YEAR) + "";//获取年份
        int month = c.get(Calendar.MONTH) + 1;//获取月份
        String monthStr = month < 10 ? "0" + month : month + "";
        int day = c.get(Calendar.DATE);//获取日
        String dayStr = day < 10 ? "0" + day : day + "";
        return Long.valueOf(yearStr + monthStr + dayStr);
    }

    /**
     * 20181130---20181201,获得下一天
     *
     * @param time
     * @return
     */
    public static Long getNext(Long time) {

        if (!isValidDate(String.valueOf(time))) {
            Loggers.APP.warn("The date {} format error , Please input the right data like yyyyMMdd ", time);
            return null;
        }
        Date date = new Date(getMillSecond(time));
        Calendar c = Calendar.getInstance();
        c.setTimeZone(china);
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        String yearStr = c.get(Calendar.YEAR) + "";//获取年份
        int month = c.get(Calendar.MONTH) + 1;//获取月份
        String monthStr = month < 10 ? "0" + month : month + "";
        int day = c.get(Calendar.DATE);//获取日
        String dayStr = day < 10 ? "0" + day : day + "";
        return Long.valueOf(yearStr + monthStr + dayStr);
    }


    /**
     * 判断参数的格式是否为“yyyyMMdd”格式的合法日期字符串
     */
    public static boolean isValidDate(String str) {
        try {
            if (str != null && !str.equals("")) {
                if (str.length() == 8) {
                    // 闰年标志
                    boolean isLeapYear = false;
                    String year = str.substring(0, 4);
                    String month = str.substring(4, 6);
                    String day = str.substring(6, 8);
                    int vYear = Integer.parseInt(year);
                    // 判断年份是否合法
                    if (vYear < 1900 || vYear > 2200) {
                        return false;
                    }
                    // 判断是否为闰年
                    if (vYear % 4 == 0 && vYear % 100 != 0 || vYear % 400 == 0) {
                        isLeapYear = true;
                    }
                    // 判断月份
                    // 1.判断月份
                    if (month.startsWith("0")) {
                        String units4Month = month.substring(1, 2);
                        int vUnits4Month = Integer.parseInt(units4Month);
                        if (vUnits4Month == 0) {
                            return false;
                        }
                        if (vUnits4Month == 2) {
                            // 获取2月的天数
                            int vDays4February = Integer.parseInt(day);
                            if (isLeapYear) {
                                if (vDays4February > 29)
                                    return false;
                            } else {
                                if (vDays4February > 28)
                                    return false;
                            }
                        }
                    } else {
                        // 2.判断非0打头的月份是否合法
                        int vMonth = Integer.parseInt(month);
                        if (vMonth != 10 && vMonth != 11 && vMonth != 12) {
                            return false;
                        }
                    }
                    // 判断日期
                    // 1.判断日期
                    if (day.startsWith("0")) {
                        String units4Day = day.substring(1, 2);
                        int vUnits4Day = Integer.parseInt(units4Day);
                        return vUnits4Day != 0;
                    } else {
                        // 2.判断非0打头的日期是否合法
                        int vDay = Integer.parseInt(day);
                        return vDay >= 10 && vDay <= 31;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            Loggers.APP.warn("Error occurs :{}", e);
            return false;
        }
    }

}
