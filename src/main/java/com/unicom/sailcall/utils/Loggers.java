package com.unicom.sailcall.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @outher larry
 * @create 2018/09/13
 */

public class Loggers {

    /**
     * APP 日志
     */
    public final static Logger APP = LoggerFactory.getLogger("com.unicom.sailcallApp");

    /**
     * 操作日志
     */
    public final static Logger OPERATE = LoggerFactory.getLogger("com.unicom.sailcallApp.operate");
}
