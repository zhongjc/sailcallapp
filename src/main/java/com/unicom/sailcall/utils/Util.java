package com.unicom.sailcall.utils;

import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * 通用工具类
 * @outher larry
 * @create 2018/09/19
 */

public class Util {

    private static Pattern EMAIL_PATTERN = Pattern.compile("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");

    /**
     * 随机 uuid, without -
     * @return
     */
    public static String randomUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 验证邮箱格式
     * @param string
     * @return
     */
    public static boolean isEmail(String string) {
        if (string == null)
            return false;
        return EMAIL_PATTERN.matcher(string).matches();
    }


    /**
     * 混淆邮件地址
     * @param email
     * @param keepLen
     * @return
     */
    public static String confuseEmail(String email, int keepLen) {
        if (StringUtils.isEmpty(email)) {
            return email;
        }

        if (email.length() < keepLen) {
            return email;
        }

        int tag = email.indexOf('@');
        char[] emailChars = email.substring(0, tag).toCharArray();

        int end = tag - keepLen / 2;
        int start = keepLen - keepLen / 2;

        StringBuilder pend = new StringBuilder();
        for (int i = 0; i < tag; i++) {
            if (i < start || i >= end) {
                pend.append(emailChars[i]);
                continue;
            }
            pend.append('*');
        }

        pend.append(email.substring(tag));

        return pend.toString();
    }

    public static String toStr(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("obj不能为空");
        }

        if (obj instanceof String) {
            String str = (String)obj;
            if (StringUtils.isEmpty(str)) {
                throw new IllegalArgumentException("obj不能为空字符串");
            }
            return str;
        }

        return String.valueOf(obj);
    }

    /**
     * 格式化
     * @param key
     * @param vars
     * @return
     */
    public static String format(String key, Object... vars){
        String[] strVars = new String[vars.length];
        for (int i = 0; i < vars.length; i++) {
            strVars[i] = Util.toStr(vars[i]);
        }
        return MessageFormat.format(key, strVars);
    }

}
