package com.unicom.sailcall.utils;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.unicom.sailcall.entity.cache.CacheInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * 分布式 session 管理
 * 将 session 保存到 redis
 * HttpServletRequest 工具类
 *
 * @outher larry
 * @create 2018/09/07
 */

@Component
public class RequestUtil {

    private static StringRedisTemplate redisTemplate;

    private static final String PREFIX_CACHE_KEY = "s:s:";
    private static final String COOKIE_NAME = "JSESSIONID";
    private static final String TOKEN_NAME = "token";

    // SESSION 超时时间  单位 秒
    private static Integer SESSION_TIMEOUT = 60*60*24*7;

    /**
     * session 获取顺序
     * 1. 从 浏览器 cookie
     * 2. 从 请求的 request headers
     * 3. 从 request parms
     *
     * 从 session 中获取用户登录信息
     * @param request
     * @return
     */
    public static CacheInfo getCacheInfo(HttpServletRequest request) {
        String token = getToken(request);

        try {
            final byte[] key = (PREFIX_CACHE_KEY + token).getBytes();
            List<Object> result = redisTemplate.executePipelined(new RedisCallback<Object>() {
                @Nullable
                @Override
                public Object doInRedis(RedisConnection connection) throws DataAccessException {
                    connection.expire(key, SESSION_TIMEOUT);
                    connection.get(key);
                    return null;
                }
            });

            if (result.size() != 2) {
                return null;
            }

            return JSON.parseObject((String) result.get(1), CacheInfo.class);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 缓存用户信息  session
     * @param response
     * @param cacheInfo
     */
    public static String cache(HttpServletResponse response, CacheInfo cacheInfo) {
        Cookie cookie = new Cookie(COOKIE_NAME, Util.randomUUID());
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setMaxAge((int)TimeUnit.DAYS.toSeconds(60*60*24*7));
        response.addCookie(cookie);

        final byte[] key = (PREFIX_CACHE_KEY + cookie.getValue()).getBytes();
        redisTemplate.executePipelined(new RedisCallback<Object>() {
            @Nullable
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                connection.set(key, JSON.toJSONBytes(cacheInfo));
                connection.expire(key, SESSION_TIMEOUT);
                return null;
            }
        });

        return cookie.getValue();
    }

    public static Cookie getTokenCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (StringUtils.equals(cookie.getName(), COOKIE_NAME)) {
                    return cookie;
                }
            }
        }
        return null;
    }


    /**
     * 获取登录标识
     * @param request
     * @return
     */
    public static String getToken(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        String token = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (StringUtils.equals(cookie.getName(), COOKIE_NAME)) {
                    token = cookie.getValue();
                    break;
                }
            }
        }

        if (StringUtils.isEmpty(token)) {
            token = request.getParameter(TOKEN_NAME);
            if (StringUtils.isEmpty(token)) {
                token = request.getHeader(TOKEN_NAME);
            }
        }
        return token;
    }


    /**
     * 退出登录 无需清除浏览器 cookie, 以服务端 session 为主
     * @param request
     */
    public static void invalidLogin(HttpServletRequest request) {
        String token = getToken(request);
        redisTemplate.delete(PREFIX_CACHE_KEY + token);
    }

    @Autowired
    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        RequestUtil.redisTemplate = redisTemplate;
    }


    @Value("${server.servlet.session.timeout}")
    public void setTimeout(Integer timeout) {
        RequestUtil.SESSION_TIMEOUT = timeout;
    }

    public static Integer getSessionTimeout() {
        return SESSION_TIMEOUT;
    }
}
