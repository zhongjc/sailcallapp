package com.unicom.sailcall;

import com.unicom.sailcall.entity.Bill;
import com.unicom.sailcall.entity.Record;
import com.unicom.sailcall.entity.returnData.RecentRecord;
import com.unicom.sailcall.entity.returnData.RecordDetail;
import com.unicom.sailcall.service.BillService;
import com.unicom.sailcall.service.RecordService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SailcallApplicationTests {
    @Autowired
    RecordService recordService;

    @Autowired
    private BillService billService;

    @Test
    public void contextLoads() {
       Record record = new Record();
       record.setSource("13800009999");
       record.setDest("13800029998");

       record.setStartTime(1538667377L);
       record.setEndTime(1538667557L);

       record.setStatus(0);
       record.setCharge(1.50);

        Integer integer = recordService.makeRecord(record);
        System.out.println(integer);
    }

    @Test
    public void test1(){
        List<Bill> userBills = billService.getUserBills(1);
        for (Bill userBill : userBills) {
            System.out.println(userBill);
        }
    }

    @Test
    public void test2(){
        List<RecordDetail> recordDetails = recordService.MonthlyRecordDetail("13800009999", 201810);
        for (RecordDetail recordDetail : recordDetails) {
            System.out.println(recordDetail);
        }
    }

    @Test
    public void test3(){
        //System.out.println(long2String((long) 1550907266));
    }

    @Test
    public void test4(){
        List<RecentRecord> recentRecords = recordService.getRecentRecords(13800009999L + "");
        for (RecentRecord recentRecord : recentRecords) {
            System.out.println(recentRecord);
        }
    }


   /* private String long2String(Long time){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        long today = cal.getTime().getTime()/1000;

        if(time >= today)
            return "今天 " + format.format(time * 1000);

        cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - 1);

        long yesterday = cal.getTime().getTime()/1000;

        if(time >= yesterday)
            return "昨天 " + format.format(time * 1000);

        Calendar cal2 = Calendar.getInstance();
        int day_of_week = cal2.get(Calendar. DAY_OF_WEEK) - 1;
       // System.out.println(day_of_week);
        if (day_of_week == 0 ) {
            day_of_week = 7 ;
        }
        cal2.add(Calendar.DATE , -day_of_week + 1 );
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.MILLISECOND, 0);

        long week = cal2.getTime().getTime()/1000;

        if(time >= week){
            String[] weekDays = { "周日", "周一", "周二", "周三", "周四", "周五", "周六" };
            cal.setTime(new Date(time * 1000));
            int w = cal.get(Calendar.DAY_OF_WEEK) - 1; // 指示一个星期中的某天。
            if (w < 0)
                w = 0;
            return weekDays[w] + " " + format.format(time * 1000);
        }

        SimpleDateFormat format1 = new SimpleDateFormat("MM/dd HH:mm");

        return format1.format(time * 1000);
    }*/





}

